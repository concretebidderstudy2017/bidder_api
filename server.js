var express = require('express');
var app = express();
var port = process.env.PORT || 1337;
var swaggerUi = require('swagger-ui-express');
var YAML = require('yamljs');
var swaggerDocument = YAML.load('./api/swagger/swagger.yaml');

mssql = require('mssql'),
db = require('./api/models/todoListModel');
bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true}));
app.use(bodyParser.json());
//app.use(SwaggerUi(swaggerExpress.runner.swagger));
app.use(function (req, res, next) {
    //Enabling CORS     
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, contentType,Content-Type, Accept, Authorization");
    next();
});

var routes = require('./api/routes/todoListRoutes'); //importing route
routes(app); //register the route

app.use('/help', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.listen(port, function() {
	console.log('to do list RESTful API server started on: ' + port);
});

