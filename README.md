# README #

A simple instruction of API

In this API, you can complete: 
 path: /projects
   get all projects from database
   post one new project to database
  
 path: /projects/:projects_id 
   get one project through projects_id
   update information of one project
   delete one project
  
 path: /elementItems
   get all of elementItems
   create one elementIItems
   
 path: /elementItems/:elementItemID
   get one elementItem
   update one elementItem
   delete one elementItem
   
   
   # There are 4 tables in the database, Factory, Project, Elements(match to"Page ElementSpecification")
   # ElementItem(match to "Page Detailed Calculation")
   
   # If you want visual tables locally,  I can help to connect to your Excel.
   