//Controllers are functions related to database
var mssql = require('mssql');
var db = require('../models/todoListModel.js');
var global_task = {};
//PROJECT API
exports.list_all_projects = function (req, res) {
  // Promise
  var call_db = new Promise(function (resolve, reject) {
    db.sql('select * from Project', function (err, task) {
      if (err) {
        res.send(err);
        return reject("can not connect db");
      }
      return resolve(task);
    });
  });

  function add_elements(task) {
    return new Promise(function (resolve, reject) {
      var callbacks = 0;
      Object.keys(task).forEach(function (key) {
        db.sql('select count(elementsid) from elements where projectid=' + task[key]['ProjectID'], function (errs, tasks) {
          task[key]['numberElements'] = tasks[0][''];
          if (++callbacks == task.length) {
            return resolve(task);
          }
        });
      });
    });
  }
  // call promise
  var call_api = function () {
    call_db
      .then(function (task) {
        console.log(task);
        return add_elements(task);
      })
      .then(function (task) {
        res.json(task);
      })
      .catch(function (error) {
        console.log(error.message);
      });
  }
  call_api();
};

exports.read_a_project = function (req, res) {
  var new_sql_string = "SELECT * from Project WHERE projectid='" + req.params.ProjectID + "'";
  db.sql(new_sql_string, function (err, task) {
    if (err) res.send(err);
    task[0]['numberElements'] = 1;
    res.json(task);
  });
};

exports.create_a_project = function (req, res) {
  var new_sql_string = "INSERT INTO Project (projectname, price, factoryname, status, customername, location) Values ('" +
    req.body.ProjectName + "','" + req.body.Price + "','" + req.body.FactoryName + "','" + req.body.Status + "','"
    + req.body.CustomerName + "','" + req.body.Location + "')"
  db.sql(new_sql_string, function (err, task) {
    if (err) res.send(err);
    res.json(task);
  });
};


exports.update_a_project = function (req, res) {
  var new_sql_string = "UPDATE Project SET projectname='" + req.body.ProjectName + "', price='" + req.body.Price + "', factoryname='" + req.body.FactoryName +
    "', status='" + req.body.Status + "', customername='" + req.body.CustomerName + "', location='" + req.body.Location +
    "' where projectid ='" + req.params.ProjectID + "'";
  db.sql(new_sql_string, function (err, task) {
    if (err) res.send(err);
    res.json(task);
  });
};

exports.delete_a_project = function (req, res) {
  db.sql("DELETE from Project WHERE projectid=" + req.params.ProjectID + "", function (err, task) {
    if (err) res.send(err);
    res.json(task);
  });
};

//ELEMENTSITEM API
exports.list_all_elementItems = function (req, res) {
  db.sql('select * from ElementItem', function (err, task) {
    if (err) res.send(err);
    res.json(task);
  });
};

exports.create_elementItem = function (req, res) {
  var new_sql_string = "INSERT INTO ElementItem (description, unit, fields, fare, quantity, amount, mytime, salary, totalcost, elementsID) Values ('" +
    req.body.Description + "','" + req.body.Unit + "','" + req.body.Fields + "','" + req.body.Fare + "','"  + req.body.Quantity + "','" + req.body.Amount +"','"
    + req.body.Mytime + "','" + req.body.Salary + "','" + req.body.TotalCost + "','" + req.body.ElementsID + "')";
  console.log(new_sql_string);
  db.sql(new_sql_string, function (err, task) {
    if (err) res.send(err);
    res.json(task);
  });
};

exports.read_elementItem = function (req, res) {
  var new_sql_string = "SELECT * from ElementItem WHERE elementitemID='" + req.params.ElementItemID + "'";
  db.sql(new_sql_string, function (err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

exports.update_elementItem = function (req, res) {
  var new_sql_string = "UPDATE ElementItem SET description='" + req.body.Description + "', unit='" + req.body.Unit + "', fields='" + req.body.Fields +
    "', fare='" + req.body.Fare + "', quantity='" + req.body.Quantity + "', amount='" + req.body.Amount + "', mytime='"
    + req.body.Mytime + "', salary='" + req.body.Salary + "', totalcost='" + req.body.TotalCost + "', elementsID='" + req.body.ElementsID +
    "' where elementitemID ='" + req.params.ElementItemID + "'";
  db.sql(new_sql_string, function (err, task) {
    if (err) res.send(err);
    res.json(task);
  });
};

exports.delete_elementItem = function (req, res) {
  db.sql("DELETE from ElementItem WHERE elementitemID=" + req.params.ElementItemID + "", function (err, task) {
    if (err) res.send(err);
    res.json(task);
  });
};

//ELEMENTS API
exports.list_all_elements = function (req, res) {
  db.sql('select * from Elements', function (err, task) {
    if (err) res.send(err);
    res.json(task);
  });
};

exports.list_elements_project = function(req,res){
  db.sql('select * from Elements where ProjectID='+req.params.ProjectID,function(err,task){
    if (err) res.send(err);
    res.json(task);
  });
}

exports.create_element = function (req, res) {
  var new_sql_string = "INSERT INTO Elements (ProductType, Number, Littera, Width, Height, Length, Area, Weight, ProductGroup, AnMarking, Img, ProjectID) Values ('" +
    req.body.ProductType + "','" + req.body.Number + "','" + req.body.Littera + "','" + req.body.Width + "','" + req.body.Height + "','" + req.body.Length + "','"
    + req.body.Area + "','"+ req.body.Weight + "','" + req.body.ProductGroup + "','" + req.body.AnMarking + "','" + req.body.Img + "','" + req.body.ProjectID + "')";
  console.log(new_sql_string);
  db.sql(new_sql_string, function (err, task) {
    if (err) res.send(err);
    res.json(task);
  });
};

exports.read_element = function (req, res) {
  var new_sql_string = "SELECT * from Elements WHERE ElementsID=" + req.params.ElementsID;
  db.sql(new_sql_string, function (err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};

exports.update_element = function (req, res) {
  var new_sql_string = "UPDATE Elements SET ProductType='" + req.body.ProductType + "', Number='" + req.body.Number + "', Littera='" + req.body.Littera +
    "', Width='" + req.body.Width + "', Height='" + req.body.Height + "', Length='" + req.body.Length + "', Area='"+ req.body.Area 
    + "', Weight='" + req.body.Weight + "', ProductGroup='" + req.body.ProductGroup + "', AnMarking='" + req.body.AnMarking +
    "', Img='" + req.body.Img +"' where ElementsID ='" + req.params.ElementsID + "'";
  console.log(new_sql_string);
  db.sql(new_sql_string, function (err, task) {
    if (err) res.send(err);
    res.json(task);
  });
};

exports.delete_element = function (req, res) {
  db.sql("DELETE from Elements WHERE ElementsID=" + req.params.ElementsID + "", function (err, task) {
    if (err) res.send(err);
    res.json(task);
  });
};

