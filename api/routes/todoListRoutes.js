'use strict';
module.exports = function (app) {
  var todoList = require('../controllers/todoListController');

  // Project Routes
  app.route('/projects')
    .get(todoList.list_all_projects)
    .post(todoList.create_a_project);

  app.route('/projects/:ProjectID')
    .get(todoList.read_a_project)
    .put(todoList.update_a_project)
    .delete(todoList.delete_a_project);

  // Elements Routes
  app.route('/elements')
    .get(todoList.list_all_elements)
    .post(todoList.create_element);

  app.route('/elements/:ElementsID')
    .get(todoList.read_element)
    .put(todoList.update_element)
    .delete(todoList.delete_element);

  app.route('/elements/project/:ProjectID')
    .get(todoList.list_elements_project);

  // Element_item Routes
  app.route('/elementItems')
    .get(todoList.list_all_elementItems)
    .post(todoList.create_elementItem);

  app.route('/elementItems/:ElementItemID')
    .get(todoList.read_elementItem)
    .put(todoList.update_elementItem)
    .delete(todoList.delete_elementItem);

};
